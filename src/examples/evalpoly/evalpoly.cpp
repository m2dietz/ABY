﻿#include <set>
#include <ENCRYPTO_utils/crypto/crypto.h>
#include <abycore/circuit/circuit.h>
#include <abycore/circuit/booleancircuits.h>
#include <abycore/sharing/arithsharing.h>
#include "../../abycore/aby/abyparty.h"

using namespace std;

void evalPoly(ABYParty *party, uint32_t bitlen, e_sharing sharing, e_role role, int n, uint32_t x, int a) {
    Circuit *circ = party->GetSharings()[sharing]->GetCircuitBuildRoutine();

    vector<share*> res;
    for (int i = 0; i < n; ++i) {
        res.push_back(circ->PutExponentiationGate(circ->PutSharedINGate(role == SERVER ? x+i : x-i, bitlen), a));
    }

    for (auto & re : res)
        re = circ->PutOUTGate(re, ALL);

    cerr << "Running circuit\n";
    party->ExecCircuit();

    cout << res[0]->get_clear_value<uint32_t>() << "\n";
    for (int i = 0; i < n; ++i)
        assert(res[i]->get_clear_value<uint32_t>() == res[0]->get_clear_value<uint32_t>());

    party->Reset();
}

int main(int argc, char **argv) {
    if (argc != 6) {
        printf("Usage: <executable> <role (0/1)> <ip address> <n> <x> <a>");
        return 1;
    }

    e_role role = (e_role) atoi(argv[1]);
    uint32_t bitlen = 32, nvals = 31, secparam = 128, nthreads = 1;
    uint16_t port = 7766;
    std::string address = argv[2];
    e_mt_gen_alg mt_alg = MT_OT;
    e_sharing sharing = S_ARITH;

    seclvl seclvl = get_sec_lvl(secparam);

    int n = atoi(argv[3]);
    uint32_t x = atoi(argv[4]);
    int a = atoi(argv[5]);

    ABYParty *party = new ABYParty(role, address, port, seclvl, bitlen, nthreads,
                                   mt_alg);

    evalPoly(party, bitlen, sharing, role, n, x, a);

    delete party;
    return 0;
}