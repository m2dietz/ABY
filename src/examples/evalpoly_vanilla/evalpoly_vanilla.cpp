﻿#include <set>
#include <ENCRYPTO_utils/crypto/crypto.h>
#include <abycore/circuit/circuit.h>
#include <abycore/circuit/booleancircuits.h>
#include <abycore/sharing/arithsharing.h>
#include "../../abycore/aby/abyparty.h"

using namespace std;

int bin[1000][1000];

share* PutExponentiationGate(Circuit *circ, uint32_t bitlen, e_role role, uint32_t x, int a) {
    vector<uint32_t> y;
    y.push_back(1);
    for (int i = 1; i <= a; ++i) y.push_back(y.back() * x);

    share *s = circ->PutCONSGate((UGATE_T) 0, bitlen);
    for (int i = 0; i <= a; ++i) {
        share *p = role == SERVER ? circ->PutDummyINGate(bitlen) : circ->PutINGate(bin[a][i] * y[i], bitlen, role);
        share *q = role == SERVER ? circ->PutINGate(y[a-i], bitlen, role) : circ->PutDummyINGate(bitlen);
        s = circ->PutADDGate(s, circ->PutMULGate(p, q));
    }
    return s;
}

void evalPoly(ABYParty *party, uint32_t bitlen, e_sharing sharing, e_role role, int n, uint32_t x, int a) {
    bin[0][0] = 1;
    for (int n = 1; n < 1000; ++n) {
        bin[n][0] = bin[n][n] = 1;
        for (int k = 1; k < n; ++k) bin[n][k] = bin[n-1][k-1] + bin[n-1][k];
    }

    Circuit *circ = party->GetSharings()[sharing]->GetCircuitBuildRoutine();

    vector<share*> res;
    for (int i = 0; i < n; ++i) {
        res.push_back(PutExponentiationGate(circ, bitlen, role, role == SERVER ? x+i : x-i, a));
    }

    for (auto & re : res)
        re = circ->PutOUTGate(re, ALL);

    cerr << "Running circuit\n";
    party->ExecCircuit();

    cout << res[0]->get_clear_value<uint32_t>() << "\n";
    for (int i = 0; i < n; ++i)
        assert(res[i]->get_clear_value<uint32_t>() == res[0]->get_clear_value<uint32_t>());

    party->Reset();
}

int main(int argc, char **argv) {
    if (argc != 6) {
        printf("Usage: <executable> <role (0/1)> <ip address> <n> <x> <a>");
        return 1;
    }

    e_role role = (e_role) atoi(argv[1]);
    uint32_t bitlen = 32, nvals = 31, secparam = 128, nthreads = 1;
    uint16_t port = 7766;
    std::string address = argv[2];
    e_mt_gen_alg mt_alg = MT_OT;
    e_sharing sharing = S_ARITH;

    seclvl seclvl = get_sec_lvl(secparam);

    int n = atoi(argv[3]);
    uint32_t x = atoi(argv[4]);
    int a = atoi(argv[5]);

    ABYParty *party = new ABYParty(role, address, port, seclvl, bitlen, nthreads,
                                   mt_alg);

    evalPoly(party, bitlen, sharing, role, n, x, a);

    delete party;
    return 0;
}